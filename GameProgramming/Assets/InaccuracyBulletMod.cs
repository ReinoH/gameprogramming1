﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public class InaccuracyBulletMod : BulletMod {
        [SerializeField] float _accuracyVariance;

        void Awake()
        {
            _accuracyVariance = _accuracyVariance / 90;
        }

        public override void Initialize(Projectile projectile)
        {
            projectile.direction += new Vector3(Random.Range(-_accuracyVariance, _accuracyVariance), 0, Random.Range(-_accuracyVariance, _accuracyVariance));
        }

    }
}
