﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public abstract class Shooter : MonoBehaviour
    {
        [SerializeField]
        internal GameObject bulletPrefab;
        [SerializeField]
        internal float fireRate;
        internal Transform spawnPoint;
        internal int bulletLayer;

        internal float shotTimer;
        

        internal virtual void Update()
        {
            if (shotTimer < fireRate)
            {
                shotTimer += Time.deltaTime;
            }
        }

        internal virtual void Awake()
        {
            spawnPoint = transform;

            if (gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                bulletLayer = LayerMask.NameToLayer("PlayerProjectile");
            } else
            {
                bulletLayer = LayerMask.NameToLayer("EnemyProjectile");
            }
        }
    }
}
