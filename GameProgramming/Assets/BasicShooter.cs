﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public class BasicShooter : Shooter {

        public virtual void Shoot()
        {
            if (shotTimer > fireRate)
            {
                shotTimer = 0;
                GameObject gameObject = SimplePool.Spawn(bulletPrefab, spawnPoint.position, transform.rotation);
                gameObject.GetComponent<Projectile>().Shoot(spawnPoint.forward);
                gameObject.SetLayer(bulletLayer);
            }
        }

    }
}
