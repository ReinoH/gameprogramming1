﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public abstract class BulletMod : MonoBehaviour
    {
        public virtual void Initialize(Projectile projectile)
        {
            
        }
        public virtual void Tick()
        {

        }
    }
}
