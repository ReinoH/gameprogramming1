﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SnappyInput {
    static KeyCode VecrticalLastPressKeyCode = KeyCode.A;
    static KeyCode HorizontalLastPressKeyCode = KeyCode.A;

    public static float GetVecticalAxis()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                return 1;
            } else
            {
                return 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                return -1;
            } else
            {
                return 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            VecrticalLastPressKeyCode = KeyCode.LeftArrow;
            return -1;
            
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            VecrticalLastPressKeyCode = KeyCode.RightArrow;
            return 1;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                if (VecrticalLastPressKeyCode == KeyCode.LeftArrow) return -1;

                return 1;
            }

            return -1;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (VecrticalLastPressKeyCode == KeyCode.RightArrow) return 1;

                return -1;
            }

            return 1;
        }

        return 0;
    }

    public static float GetHorizontalAxis()
    {
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            HorizontalLastPressKeyCode = KeyCode.DownArrow;
            return -1;

        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            HorizontalLastPressKeyCode = KeyCode.UpArrow;
            return 1;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (HorizontalLastPressKeyCode == KeyCode.DownArrow) return -1;

                return 1;
            }

            return -1;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                if (HorizontalLastPressKeyCode == KeyCode.UpArrow) return 1;

                return -1;
            }

            return 1;
        }

        return 0;
    }
}
