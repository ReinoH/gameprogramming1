﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailRendererSpawningDelay : MonoBehaviour {
    private bool _hasSet = false;
    private TrailRenderer trailRenderer;

    void Awake()
    {
        trailRenderer = GetComponent<TrailRenderer>();
    }
 
	void OnEnable()
    {
        StartCoroutine(SpawnFrameDelay());
    }

    void OnDisable()
    {
    }

    IEnumerator SpawnFrameDelay()
    {
        trailRenderer.time = 0;
        yield return new WaitForEndOfFrame();
        trailRenderer.time = 0.4f;
        yield return null;

    }
}
