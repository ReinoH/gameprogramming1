﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnInstanceData {

    public int prefabNumberSpawned;
    public int numberSpawned;
    public int[] spawnPositions;
    public float delayToNextSpawnInstance = 0f;
}
