﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WaveEndType
{
    TimeDelay, EnemiesKilled
}

public class SpawnWaveData : ScriptableObject {

    public WaveEndType waveEndCondition = WaveEndType.TimeDelay;
    public float delayToNextWave = 0;
    public bool killAllEnemies = true;
    public int numberOfEnemiesToKill = 0;
    public bool loopWave = false;

    //This is our list we want to use to represent our class as an array.
    public List<SpawnInstanceData> MyList = new List<SpawnInstanceData>(1);


    void AddNew()
    {
        //Add a new index position to the end of our list
        MyList.Add(new SpawnInstanceData());
    }

    void Remove(int index)
    {
        //Remove an index position from our list at a point in our list array
        MyList.RemoveAt(index);
    }

}
