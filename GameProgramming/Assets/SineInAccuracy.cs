﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public class SineInAccuracy : BulletMod {
        [SerializeField] float _accuracyVariance;
        [SerializeField]
        private float _frequency;

        void Awake()
        {
            _accuracyVariance = _accuracyVariance / 90;
        }

        public override void Initialize(Projectile projectile)
        {
            projectile.direction += new Vector3(Mathf.Sin(Time.time * _frequency) * _accuracyVariance, 0, Mathf.Sin(Time.time * _frequency) * _accuracyVariance);
        }

    }
}
