﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TamkShooter
{
    public class HealthChangedEventArgs : EventArgs
    {
        public int CurrentHealth { get; private set; }

        public HealthChangedEventArgs(int currentHealth)
        {
            CurrentHealth = currentHealth;
        }

    }

    public delegate void HealthChangedDelegate(object sender, HealthChangedEventArgs args);

    public interface IHealth
    {
        int currentHealth { get; set; }
        bool TakeDamage(int damage);

        event HealthChangedDelegate HealthChanged;
    }
}
