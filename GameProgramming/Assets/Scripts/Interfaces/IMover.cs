﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TamkShooter
{
    public interface IMover
    {
        Vector3 position { get; set; }
        Quaternion rotation { get; set; }

        float Speed { get; set; }

        void MoveTowardsPosition(Vector3 targetPosition);

        void MoveToDirection(Vector3 targetPosition);

        void RotateTowardsPosition(Vector3 targetPosition);
    }
}
