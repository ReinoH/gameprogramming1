﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TamkShooter
{
    public interface IReasoner
    {
        void Reason();
        void Act();
    }
}
