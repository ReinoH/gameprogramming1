﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TamkShooter
{
    public class Health : MonoBehaviour, IHealth
    {
        [SerializeField] private int _health;

        public int currentHealth
        {
            get
            {
                return _health;
            }

            set
            {
                _health = Mathf.Clamp(value, 0, value);
                if (HealthChanged != null)
                {
                    HealthChanged(this, new HealthChangedEventArgs(_health));
                }
            }
        }

        public event HealthChangedDelegate HealthChanged;

        public bool TakeDamage(int damage)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
