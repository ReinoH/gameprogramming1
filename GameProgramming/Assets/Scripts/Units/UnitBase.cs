﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public abstract class UnitBase : MonoBehaviour
    {
        internal IMover Movement;
        internal IReasoner Reasoner;

        public IHealth Health { get; protected set; }

        protected virtual void Awake()
        {
            Health = gameObject.GetOrAddComponent<Health>();
            Movement = gameObject.GetOrAddComponent<Mover>();
        }

        public void TakeDamage(int amount)
        {
            if (Health.TakeDamage(amount))
            {
                Die();
            }
        }

        public abstract void Die();
        public abstract int ProjectileLayer { get; }

    }
}
