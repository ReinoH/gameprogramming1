﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TamkShooter
{
    class PlayerUnit : UnitBase
    {
        private IMover slowDownMovement;
        [SerializeField]
        BasicShooter[] normalShooters;
        [SerializeField] BasicShooter[] slowDownShooters;
        Renderer hitBoxRenderer;

        private List<BaseState> playerStates = new List<BaseState>();
        private BaseState currentState;

        public enum UnitType
        {
            None = 0,
            Progressive = 1,
            Hero = 2,
            Doom = 3
        }

        public enum PlayerState
        {
            Moving = 0,
            SlowDown = 1,
            Paused = 2,
        }

        public void ChangePlayerState(PlayerState stateToMoveTo) 
        {
            foreach(BaseState state in playerStates)
            {
                if (state.stateID == stateToMoveTo)
                {
                    currentState.OnLeave();
                    currentState = state;
                    currentState.OnEnter();
                    return;
                }
            }
        }

        protected virtual void Awake()
        {
            Health = gameObject.GetOrAddComponent<Health>();
            Movement = gameObject.GetOrAddComponent<Mover>();
            slowDownMovement = gameObject.AddComponent<Mover>();
            slowDownMovement.Speed = slowDownMovement.Speed = Movement.Speed / 2;
        }

        public override int ProjectileLayer
        {
            get
            {
                return LayerMask.NameToLayer(Constants.playerProjectileName);
            }
        }

        public override void Die()
        {
            // TODO: Dying code comes here.
        }

        public abstract class BaseState
        {
            public PlayerState stateID
            {
                get;
                private set;
            }
            public virtual void HandleInput()
            {

            }
            public virtual void OnEnter()
            {

            }
            public virtual void OnLeave()
            {

            }
        }

        public class MovementState : BaseState
        {
            public override void HandleInput()
            {

            }
        }

        public class SlowDownState : BaseState
        {
            public override void HandleInput()
            {

            }
        }

        protected void Update()
        {
            if (Input.GetButton("SlowDown"))
            {
                slowDownMovement.MoveToDirection(new Vector3(SnappyInput.GetVecticalAxis(), 0, SnappyInput.GetHorizontalAxis()));

                if (Input.GetButton("Fire1"))
                {
                    foreach (BasicShooter shooter in slowDownShooters)
                    {
                        shooter.Shoot();
                    }
                }
                

            } else
            {
                Movement.MoveToDirection(new Vector3(SnappyInput.GetVecticalAxis(), 0, SnappyInput.GetHorizontalAxis()));
                
                if (Input.GetButton("Fire1"))
                {
                    foreach (BasicShooter shooter in normalShooters)
                    {
                        shooter.Shoot();
                    }
                }

                
            }

            
            
        }
    }
}
