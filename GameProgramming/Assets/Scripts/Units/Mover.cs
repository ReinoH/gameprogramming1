﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TamkShooter
{
    class Mover : MonoBehaviour, IMover
    {
        [SerializeField]
        private float _speed;

        public Vector3 position
        {
            get
            {
                return transform.position;
            }

            set
            {
                transform.position = value;
            }
        }

        public Quaternion rotation
        {
            get
            {
                return transform.rotation;
            }

            set
            {
                transform.rotation = rotation;
            }
        }

        public float Speed
        {
            get
            {
                return _speed;
            }

            set
            {
                _speed = value;
            }
        }

        public void MoveToDirection(Vector3 direction)
        {
            direction = direction.normalized;
            position += direction * Speed * Time.deltaTime;
        }

        public void MoveTowardsPosition(Vector3 targetPosition)
        {
            Vector3 direction = (targetPosition - position);
            MoveToDirection(direction);

        }

        public void RotateTowardsPosition(Vector3 targetPosition)
        {
            Vector3 direction = targetPosition - position;
            direction.y = position.y;
            direction = direction.normalized;
            Vector3 rotation = Vector3.RotateTowards(transform.forward, direction, Speed * Time.deltaTime, 0);
            this.rotation = Quaternion.LookRotation(rotation);
        }
    }
}
