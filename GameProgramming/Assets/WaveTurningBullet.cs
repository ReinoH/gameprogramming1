﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveTurningBullet : MonoBehaviour {
    public float StartSpeed;
    public float FullSpeed;
    public float AccelerationTime;
    public AnimationCurve AccelerationCurve;

    public float CurvingSpeed;

    void Start()
    {

    }

    void Update()
    {
        transform.Rotate(new Vector3(0, CurvingSpeed * Time.deltaTime, 0));
        transform.position += transform.forward * StartSpeed * Time.deltaTime; 
    }


}
