﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SpawnWaveData)), CanEditMultipleObjects]
public class SpawnWaveDataEditor : Editor
{

    public SerializedProperty
        waveEndCondition_Prop,
        delayToNextWave_Prop,
        loopWave_Prop,
        numberOfEnemiesToKill_Prop,
        killAllEnemies_Prop,
        spawnInstances_Prop;

    enum displayFieldType { DisplayAsAutomaticFields, DisplayAsCustomizableGUIFields }
    displayFieldType DisplayFieldType;

    SpawnWaveData t;
    SerializedObject GetTarget;
    SerializedProperty ThisList;
    int ListSize;

    void OnEnable()
    {
        t = (SpawnWaveData)target;
        GetTarget = new SerializedObject(t);
        ThisList = GetTarget.FindProperty("MyList"); // Find the List in our script and create a refrence of it

        // Setup the SerializedProperties
        waveEndCondition_Prop = GetTarget.FindProperty("waveEndCondition");
        delayToNextWave_Prop = GetTarget.FindProperty("delayToNextWave");
        loopWave_Prop = GetTarget.FindProperty("loopWave");
        numberOfEnemiesToKill_Prop = GetTarget.FindProperty("numberOfEnemiesToKill");
        killAllEnemies_Prop = GetTarget.FindProperty("killAllEnemies");
        spawnInstances_Prop = GetTarget.FindProperty("MyList");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(waveEndCondition_Prop);

        WaveEndType st = (WaveEndType)waveEndCondition_Prop.enumValueIndex;

        switch (st)
        {
            case WaveEndType.TimeDelay:
                EditorGUILayout.PropertyField(delayToNextWave_Prop, new GUIContent("DelayToNextWave"));
                EditorGUILayout.PropertyField(loopWave_Prop, new GUIContent("LoopWave"));
                break;

            case WaveEndType.EnemiesKilled:
                EditorGUILayout.PropertyField(killAllEnemies_Prop);

                bool needToKillAll = killAllEnemies_Prop.boolValue;

                if (needToKillAll)
                {

                } else
                {
                    EditorGUILayout.IntSlider(numberOfEnemiesToKill_Prop, 0, 999, new GUIContent("Kills Needed"));
                    EditorGUILayout.PropertyField(loopWave_Prop, new GUIContent("LoopWave"));
                }
                break;
        }

        EditorGUILayout.PropertyField(spawnInstances_Prop, true);

        serializedObject.ApplyModifiedProperties();

        GetTarget.Update();

        //Resize our list
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Define the list size with a number");
        ListSize = ThisList.arraySize;
        ListSize = EditorGUILayout.IntField("List Size", ListSize);

        if (ListSize != ThisList.arraySize)
        {
            while (ListSize > ThisList.arraySize)
            {
                ThisList.InsertArrayElementAtIndex(ThisList.arraySize);
            }
            while (ListSize < ThisList.arraySize)
            {
                ThisList.DeleteArrayElementAtIndex(ThisList.arraySize - 1);
            }
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Or");
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        //Or add a new item to the List<> with a button
        EditorGUILayout.LabelField("Add a new item with a button");

        if (GUILayout.Button("Add New"))
        {
            t.MyList.Add(new SpawnInstanceData());
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        //Display our list to the inspector window

        for (int i = 0; i < ThisList.arraySize; i++)
        {
            SerializedProperty MyListRef = ThisList.GetArrayElementAtIndex(i);
            SerializedProperty spawnedNumber = MyListRef.FindPropertyRelative("prefabNumberSpawned");
            SerializedProperty delayToNextSpawnInstance = MyListRef.FindPropertyRelative("delayToNextSpawnInstance");
            SerializedProperty spawnPositionArray = MyListRef.FindPropertyRelative("spawnPositions");


            // Display the property fields in two ways.

            if (DisplayFieldType == 0)
            {// Choose to display automatic or custom field types. This is only for example to help display automatic and custom fields.
                //1. Automatic, No customization <-- Choose me I'm automatic and easy to setup
                EditorGUILayout.LabelField("Automatic Field By Property Type");
                EditorGUILayout.PropertyField(spawnedNumber);
                EditorGUILayout.PropertyField(delayToNextSpawnInstance);

                // Array fields with remove at index
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Array Fields");

                if (GUILayout.Button("Add New Index", GUILayout.MaxWidth(130), GUILayout.MaxHeight(20)))
                {
                    spawnPositionArray.InsertArrayElementAtIndex(spawnPositionArray.arraySize);
                    spawnPositionArray.GetArrayElementAtIndex(spawnPositionArray.arraySize - 1).intValue = 0;
                }

                for (int a = 0; a < spawnPositionArray.arraySize; a++)
                {
                    EditorGUILayout.PropertyField(spawnPositionArray.GetArrayElementAtIndex(a));
                    if (GUILayout.Button("Remove  (" + a.ToString() + ")", GUILayout.MaxWidth(100), GUILayout.MaxHeight(15)))
                    {
                        spawnPositionArray.DeleteArrayElementAtIndex(a);
                    }
                }
            }
            else
            {
                //Or

                //2 : Full custom GUI Layout <-- Choose me I can be fully customized with GUI options.
                EditorGUILayout.LabelField("Customizable Field With GUI");
                spawnedNumber.intValue = EditorGUILayout.IntField("My Custom Int", spawnedNumber.intValue);
                delayToNextSpawnInstance.floatValue = EditorGUILayout.FloatField("My Custom Float", delayToNextSpawnInstance.floatValue);


                // Array fields with remove at index
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Array Fields");

                if (GUILayout.Button("Add New Index", GUILayout.MaxWidth(130), GUILayout.MaxHeight(20)))
                {
                    spawnPositionArray.InsertArrayElementAtIndex(spawnPositionArray.arraySize);
                    spawnPositionArray.GetArrayElementAtIndex(spawnPositionArray.arraySize - 1).intValue = 0;
                }

                for (int a = 0; a < spawnPositionArray.arraySize; a++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("My Custom Int (" + a.ToString() + ")", GUILayout.MaxWidth(120));
                    spawnPositionArray.GetArrayElementAtIndex(a).intValue = EditorGUILayout.IntField("", spawnPositionArray.GetArrayElementAtIndex(a).intValue, GUILayout.MaxWidth(100));
                    if (GUILayout.Button("-", GUILayout.MaxWidth(15), GUILayout.MaxHeight(15)))
                    {
                        spawnPositionArray.DeleteArrayElementAtIndex(a);
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.Space();

            //Remove this index from the List
            EditorGUILayout.LabelField("Remove an index from the List<> with a button");
            if (GUILayout.Button("Remove This Index (" + i.ToString() + ")"))
            {
                ThisList.DeleteArrayElementAtIndex(i);
            }
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        //Apply the changes to our list
        GetTarget.ApplyModifiedProperties();
    }
}
