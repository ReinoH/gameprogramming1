﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TamkShooter
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float _shootingSpeed;
        [SerializeField] private float _beginningSpeed;
        [SerializeField] private float _damage;
        [SerializeField] private float _accelerationTime;
        [SerializeField] EasingType _accelerationEasing;
        [SerializeField] private float _despawnTime;

        private IMover mover;
        private float accelerationTimer;
        private float despawnTimer = 0;
        [HideInInspector] public Vector3 direction;
        private BulletMod[] bulletMods;

        // Use this for initialization
        protected virtual void Awake()
        {
            mover = GetComponent<Mover>();
            bulletMods = GetComponents<BulletMod>();
        }

        // Update is called once per frame
        internal virtual void Update()
        {
            if ( accelerationTimer <= 1)
            {
                accelerationTimer += Time.deltaTime / _accelerationTime;
                mover.Speed = Mathf.Lerp(_beginningSpeed, _shootingSpeed, Easing.EaseIn(accelerationTimer, _accelerationEasing));
            }

            despawnTimer += Time.deltaTime;

            if (despawnTimer >= _despawnTime)
            {
                despawnTimer = 0;
                SimplePool.Despawn(gameObject);
            }

            mover.MoveToDirection(direction);
        }

        void OnDisable()
        {
            despawnTimer = 0;
            accelerationTimer = 0;
            mover.Speed = 0;
    }

        public virtual void Shoot(Vector3 direction)
        {
            this.direction = direction;

            foreach (BulletMod mod in bulletMods)
            {
                mod.Initialize(this);
            }

            
        }
    }
}
